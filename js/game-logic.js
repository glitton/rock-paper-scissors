//Logic for Rock paper scissors game as seen here
//https://s3.amazonaws.com/codecademy-content/programs/build-apis/projects/build-apis-project-1-rock-paper-scissors-x99/project/index.html

// Twelve global variables representing each player's move types and values
let playerOneMoveOneType;
let playerOneMoveOneValue;
let playerOneMoveTwoType;
let playerOneMoveTwoValue;
let playerOneMoveThreeType;
let playerOneMoveThreeValue;
let playerTwoMoveOneType;
let playerTwoMoveOneValue;
let playerTwoMoveTwoType;
let playerTwoMoveTwoValue;
let playerTwoMoveThreeType;
let playerTwoMoveThreeValue;

const ROCK = "rock";
const PAPER = "paper";
const SCISSORS = "scissors";
const TIE = "Tie";
const PLAYER1 = "Player One";
const PLAYER2 = "Player Two";

const setPlayerMoves = (
  player,
  m1type,
  m1value,
  m2type,
  m2value,
  m3type,
  m3value
) => {
  //Edge case: no move if no type, value is provided
  if (!m1type || !m1value || !m2type || !m2value || !m3type || !m3value) {
    return;
  }
  const validType = type =>
    type === ROCK || type === PAPER || type === SCISSORS;
  const validTypes = (t1, t2, t3) =>
    validType(t1) && validType(t2) && validType(t3);
  const validValues = (v1, v2, v3) =>
    v1 >= 1 && v2 >= 1 && v3 >= 1 && v1 + v2 + v3 <= 99;

  if (
    !validTypes(m1type, m2type, m3type) ||
    !validValues(m1value, m2value, m3value)
  ) {
    return;
  }
  //should set player one\'s moves with valid inputs
  switch (player) {
    case PLAYER1:
      playerOneMoveOneType = m1type;
      playerOneMoveOneValue = m1value;
      playerOneMoveTwoType = m2type;
      playerOneMoveTwoValue = m2value;
      playerOneMoveThreeType = m3type;
      playerOneMoveThreeValue = m3value;
      break;
    case PLAYER2:
      playerTwoMoveOneType = m1type;
      playerTwoMoveOneValue = m1value;
      playerTwoMoveTwoType = m2type;
      playerTwoMoveTwoValue = m2value;
      playerTwoMoveThreeType = m3type;
      playerTwoMoveThreeValue = m3value;
      break;
  }
};

const getRoundWinner = round => {
  let p1type;
  let p2type;
  let p1value;
  let p2value;
  //Edge case: if round value is greater than 4, it is invalid
  if (round >= 4) {
    return null;
  }

  switch (round) {
    case 1:
      p1type = playerOneMoveOneType;
      p1value = playerOneMoveOneValue;
      p2type = playerTwoMoveOneType;
      p2value = playerTwoMoveOneValue;
      break;
    case 2:
      p1type = playerOneMoveTwoType;
      p1value = playerOneMoveTwoValue;
      p2type = playerTwoMoveTwoType;
      p2value = playerTwoMoveTwoValue;
      break;
    case 3:
      p1type = playerOneMoveThreeType;
      p1value = playerOneMoveThreeValue;
      p2type = playerTwoMoveThreeType;
      p2value = playerTwoMoveThreeValue;
      break;
  }
  return evaluateMove(p1type, p1value, p2type, p2value);
};

const evaluateMove = (p1type, p1value, p2type, p2value) => {
  //Edge case: if invalid move type or value, return null

  if (!p1type || !p2type || !p1value || !p2value) {
    return null;
  }
  //Return the right player in case of the same type, higher value wins
  if (p1type === p2type) {
    if (p1value === p2value) {
      return TIE;
    } else if (p1value > p2value) {
      return PLAYER1;
    }
    return PLAYER2;
  }
  //Different types, usual RPS rules apply
  switch (p1type) {
    case ROCK:
      return p2type === PAPER ? PLAYER2 : PLAYER1;
      break;

    case PAPER:
      return p2type === ROCK ? PLAYER1 : PLAYER2;
      break;

    case SCISSORS:
      return p2type === ROCK ? PLAYER2 : PLAYER1;
      break;
  }
};

let p1WinCount;
let p2WinCount;

const getGameWinner = () => {
  let r1winner = getRoundWinner(1);
  let r2winner = getRoundWinner(2);
  let r3winner = getRoundWinner(3);
  p1WinCount = 0;
  p2WinCount = 0;

  if (!r1winner || !r2winner || !r3winner) {
    return null;
  }
  addToCount(r1winner);
  addToCount(r2winner);
  addToCount(r3winner);

  if (p1WinCount === p2WinCount) {
    return TIE;
  }
  if (p1WinCount > p2WinCount) {
    return PLAYER1;
  }
  return PLAYER2;
};

const addToCount = winner => {
  switch (winner) {
    case PLAYER1:
      p1WinCount += 1;
      break;
    case PLAYER2:
      p2WinCount += 1;
      break;
  }
};

const setComputerMoves = () => {
  setP2MoveTypes();
  setP2MoveValues();
};

const setP2MoveTypes = () => {
  playerTwoMoveOneType = getRandomType();
  playerTwoMoveTwoType = getRandomType();
  playerTwoMoveThreeType = getRandomType();
};

const getRandomNum = num => Math.floor(Math.random() * num);

const getRandomType = () => {
  switch (getRandomNum(3)) {
    case 0:
      return ROCK;
    case 1:
      return PAPER;
    case 2:
      return SCISSORS;
  }
};

const setP2MoveValues = () => {
  //each value must be greater than 1 leaving 96 to distribute randomly
  let remainder = 96;
  let portion = getRandomNum(remainder);
  playerTwoMoveOneValue = portion + 1;
  remainder -= portion;

  portion = getRandomNum(remainder);
  playerTwoMoveTwoValue = portion + 1;
  remainder -= portion;

  playerTwoMoveThreeValue = remainder + 1;
};
